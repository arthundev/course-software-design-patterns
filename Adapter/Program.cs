﻿using Adapter;
using Core.Books;
using Infrastructure.DataSource;
using Infrastructure.JsonSerializer;
using System.Diagnostics;

const string printedBookIsbn = "978-0-307-45563-2";

var printedBook = BookProductDataSource.GetSingleBookProduct<PrintedBook>(printedBookIsbn);
Debug.WriteLine(JsonSerializerConfig.Serialize(printedBook));

IBookProductOverview printedBookOverviewAdapter = new BookProductAdapter(printedBook);
Debug.WriteLine(JsonSerializerConfig.Serialize(printedBookOverviewAdapter.GetBookOverview()));