﻿using Core.Contracts;
using Infrastructure.DataSource;

namespace Adapter;

public interface IBookProductOverview
{
    IBookProduct GetBookOverview();
}

public class BookProductAdapter : IBookProductOverview
{
    private IBookProduct BookProduct { get; }

    public BookProductAdapter(IBookProduct bookProduct)
    {
        BookProduct = bookProduct;
    }

    public IBookProduct GetBookOverview()
    {
        SetBookProductRating();
        SetBookProductReviews();
        return BookProduct;
    }

    private void SetBookProductRating()
    {
        // business logic to get book rating
        var bookProductRating = BookProductRatingDataSource.GetBookProductRating(BookProduct.Isbn);
        BookProduct.SetRating(bookProductRating.Average);
    }

    private void SetBookProductReviews()
    {
        // business logic to get book reviews
        var bookProductReviews = BookProductReviewDataSource.GetBookProductReviews(BookProduct.Isbn);
        BookProduct.SetReviews(bookProductReviews);
    }
}