﻿namespace Strategy.Enums;

public enum SortTypeEnum
{
    Ascending,
    Descending
}