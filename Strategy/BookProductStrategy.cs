﻿using Core.Contracts;
using Strategy.Enums;

namespace Strategy;

public interface IBookProductSortStrategy
{
    IEnumerable<IBookProduct> SortBookProducts(IEnumerable<IBookProduct> bookProducts, SortTypeEnum sortType);
}

public class BookProductStrategy
{
    private IBookProductSortStrategy? _strategy;

    public void SetStrategy(IBookProductSortStrategy strategy)
    {
        _strategy = strategy;
    }

    public IEnumerable<IBookProduct> ExecuteStrategy(IEnumerable<IBookProduct> bookProducts, SortTypeEnum sortType)
    {
        return _strategy != null
            ? _strategy.SortBookProducts(bookProducts, sortType)
            : Array.Empty<IBookProduct>();
    }
}