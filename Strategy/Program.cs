﻿using Core.Books;
using Core.Enums;
using Infrastructure.DataSource;
using Infrastructure.JsonSerializer;
using Strategy;
using Strategy.Enums;
using Strategy.Impl;
using System.Diagnostics;

var bookProducts = BookProductDataSource.GetBookProducts<PrintedBook>(BookTypeEnum.PrintedBook).ToArray();

Debug.WriteLine($"==Book Products sorted default==\n{JsonSerializerConfig.Serialize(bookProducts)}");

var bookProductStrategy = new BookProductStrategy();

bookProductStrategy.SetStrategy(new BookProductSortByTitle());
var sortedBookProducts = bookProductStrategy.ExecuteStrategy(bookProducts, SortTypeEnum.Descending);
Debug.WriteLine($"==Book Products sorted by title descending==\n{JsonSerializerConfig.Serialize(sortedBookProducts)}");

bookProductStrategy.SetStrategy(new BookProductSortByPrice());
sortedBookProducts = bookProductStrategy.ExecuteStrategy(bookProducts, SortTypeEnum.Ascending);
Debug.WriteLine($"==Book Products sorted by price ascending==\n{JsonSerializerConfig.Serialize(sortedBookProducts)}");