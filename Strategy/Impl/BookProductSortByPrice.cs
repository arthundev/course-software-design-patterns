﻿using Core.Contracts;
using Strategy.Enums;

namespace Strategy.Impl;

public class BookProductSortByPrice : IBookProductSortStrategy
{
    public IEnumerable<IBookProduct> SortBookProducts(IEnumerable<IBookProduct> bookProducts, SortTypeEnum sortType)
        => sortType switch
        {
            SortTypeEnum.Ascending => bookProducts.OrderBy(book => book.Price),
            SortTypeEnum.Descending => bookProducts.OrderByDescending(book => book.Price),
            _ => bookProducts
        };
}