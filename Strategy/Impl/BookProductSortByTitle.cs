﻿using Core.Contracts;
using Strategy.Enums;

namespace Strategy.Impl;

public class BookProductSortByTitle : IBookProductSortStrategy
{
    public IEnumerable<IBookProduct> SortBookProducts(IEnumerable<IBookProduct> bookProducts, SortTypeEnum sortType)
        => sortType switch
        {
            SortTypeEnum.Ascending => bookProducts.OrderBy(book => book.Title),
            SortTypeEnum.Descending => bookProducts.OrderByDescending(book => book.Title),
            _ => bookProducts
        };
}