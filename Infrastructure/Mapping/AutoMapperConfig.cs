﻿using AutoMapper;
using Core.Books;
using Infrastructure.DataSource;

namespace Infrastructure.Mapping;

public static class AutoMapperConfig
{
    public static IMapper Mapper => GetMapper();

    private static IMapper GetMapper()
    {
        var predicate = PredicateMapperConfiguration();
        var config = new MapperConfiguration(predicate);
        return config.CreateMapper();
    }

    private static Action<IMapperConfigurationExpression> PredicateMapperConfiguration()
        => config =>
        {
            config.CreateMap<BookProductData, BookProduct>();
            config.CreateMap<BookProductData, PrintedBook>();
            config.CreateMap<BookProductData, AudioBook>();
        };
}