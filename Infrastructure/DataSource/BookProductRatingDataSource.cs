﻿namespace Infrastructure.DataSource;

public static class BookProductRatingDataSource
{
    private static readonly BookRatingData[] BookProductRatings =
    [
        new BookRatingData
        {
            Isbn = "978-0-307-45563-2",
            Average = 4.11f,
            TotalRatings = 979814,
            TotalReviews = 46620
        },
        new BookRatingData
        {
            Isbn = "978-0-7679-0342-7",
            Average = 4.47f,
            TotalRatings = 10151372,
            TotalReviews = 164107
        }
    ];

    public static BookRatingData GetBookProductRating(string? isbn)
    {
        if (BookProductRatings.SingleOrDefault(book => book.Isbn == isbn) is { } bookProductRating)
            return bookProductRating;
        throw new Exception($"Book rating with ISBN '{isbn}' was not found.");
    }
}

public record BookRatingData
{
    public required string Isbn { get; init; }
    public required float Average { get; init; }
    public required long TotalRatings { get; init; }
    public required long TotalReviews { get; init; }
}