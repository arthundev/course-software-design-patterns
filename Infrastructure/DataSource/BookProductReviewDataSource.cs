﻿namespace Infrastructure.DataSource;

public static class BookProductReviewDataSource
{
    private static readonly BookReviewData[] BookProductReviews =
    [
        new BookReviewData
        {
            Isbn = "978-0-307-45563-2",
            Review = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        },
        new BookReviewData
        {
            Isbn = "978-0-307-45563-2",
            Review = "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
        },
        new BookReviewData
        {
            Isbn = "978-0-7679-0342-7",
            Review = "Ut enim ad minim veniam"
        },
        new BookReviewData
        {
            Isbn = "978-0-7679-0342-7",
            Review = "Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"
        }
    ];

    public static string[] GetBookProductReviews(string? isbn)
    {
        return BookProductReviews
            .Where(book => book.Isbn == isbn)
            .Select(book => book.Review)
            .ToArray();
    }
}

public record BookReviewData
{
    public required string Isbn { get; init; }
    public required string Review { get; init; }
}