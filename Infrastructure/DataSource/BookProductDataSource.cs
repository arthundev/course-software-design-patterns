﻿using Core.Enums;
using Infrastructure.Mapping;

namespace Infrastructure.DataSource;

public static class BookProductDataSource
{
    private static readonly BookProductData[] BookProducts =
    [
        new BookProductData
        {
            Type = BookTypeEnum.PrintedBook,
            Isbn = "978-0-307-45563-2",
            Title = "Cien años de soledad",
            Author = "Gabriel García Márquez",
            Price = 19.95m,
            PageCount = 471
        },
        new BookProductData
        {
            Type = BookTypeEnum.AudioBook,
            Isbn = "978-0-7679-0342-7",
            Title = "Harry Potter y la piedra filosofal",
            Author = "J.K. Rowling",
            Price = 14.95m,
            RunningTime = 29229
        },
        new BookProductData
        {
            Type = BookTypeEnum.PrintedBook,
            Isbn = "978-84-206-5669-9",
            Title = "El Principito",
            Author = "Antoine de Saint-Exupéry",
            Price = 10.95m
        },
        new BookProductData
        {
            Type = BookTypeEnum.PrintedBook,
            Isbn = "978-84-9992-702-7",
            Title = "Sapiens: Una breve historia de la humanidad",
            Author = "Yuval Noah Harari",
            Price = 22.95m
        },
        new BookProductData
        {
            Type = BookTypeEnum.AudioBook,
            Isbn = "978-84-9892-082-7",
            Title = "El Señor de los Anillos",
            Author = "J.R.R. Tolkien",
            Price = 34.95m,
            RunningTime = 151200
        },
        new BookProductData
        {
            Type = BookTypeEnum.AudioBook,
            Isbn = "978-84-663-4223-8",
            Title = "El Alquimista",
            Author = "Paulo Coelho",
            Price = 16.95m,
            RunningTime = 39780
        }
    ];

    private static BookProductData? GetSingleBookProduct(string isbn)
    {
        var predicate = FilterSingleBookProductByIsbn(isbn);
        return BookProducts.SingleOrDefault(predicate);
    }

    public static BookProductData? GetSingleBookProduct(string isbn, BookTypeEnum bookType)
    {
        var predicate = FilterSingleBookProductByIsbnAndType(isbn, bookType);
        return BookProducts.SingleOrDefault(predicate);
    }

    public static T GetSingleBookProduct<T>(string isbn)
    {
        if (GetSingleBookProduct(isbn) is { } bookProduct)
            return AutoMapperConfig.Mapper.Map<T>(bookProduct);
        throw new Exception($"Book with ISBN '{isbn}' was not found.");
    }

    public static IEnumerable<T> GetBookProducts<T>(BookTypeEnum bookType)
    {
        var bookPorducts = BookProducts.Where(book => book.Type == bookType);
        return AutoMapperConfig.Mapper.Map<IEnumerable<T>>(bookPorducts);
    }

    private static Func<BookProductData, bool> FilterSingleBookProductByIsbn(string isbn)
        => book => book.Isbn == isbn;

    private static Func<BookProductData, bool> FilterSingleBookProductByIsbnAndType(string isbn, BookTypeEnum bookType)
        => book => book.Isbn == isbn && book.Type == bookType;
}

public record BookProductData
{
    public BookTypeEnum Type { get; init; }
    public required string Isbn { get; init; }
    public required string Title { get; init; }
    public required string Author { get; init; }
    public required decimal Price { get; init; }
    public int? PageCount { get; init; }
    public int? RunningTime { get; init; }
}