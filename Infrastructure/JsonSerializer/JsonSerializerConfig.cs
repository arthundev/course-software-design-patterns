﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Infrastructure.JsonSerializer;

public static class JsonSerializerConfig
{
    private static readonly JsonSerializerOptions SerializerOptions = new()
    {
        WriteIndented = true,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
        Converters = { new JsonStringEnumConverter() }
    };

    public static string Serialize<TValue>(TValue value)
    {
        return System.Text.Json.JsonSerializer.Serialize(value, SerializerOptions);
    }
}