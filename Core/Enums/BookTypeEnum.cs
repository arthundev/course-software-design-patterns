﻿namespace Core.Enums;

public enum BookTypeEnum
{
    PrintedBook,
    AudioBook
}