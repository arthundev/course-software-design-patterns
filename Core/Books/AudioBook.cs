﻿using Core.Enums;

namespace Core.Books;

public class AudioBook : BookProduct
{
    public override BookTypeEnum Type => BookTypeEnum.AudioBook;
    public required int RunningTime { get; init; }
}