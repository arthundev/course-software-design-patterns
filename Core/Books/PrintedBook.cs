﻿using Core.Enums;

namespace Core.Books;

public class PrintedBook : BookProduct
{
    public override BookTypeEnum Type => BookTypeEnum.PrintedBook;
    public required int PageCount { get; init; }
}