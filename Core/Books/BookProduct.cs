﻿using Core.Contracts;
using Core.Enums;

namespace Core.Books;

public abstract class BookProduct : IBookProduct
{
    public virtual BookTypeEnum Type { get; init; }
    public string? Isbn { get; init; }
    public string? Title { get; init; }
    public string? Author { get; init; }
    public decimal? Price { get; init; }
    public float? Rating { get; private set; }
    public IReadOnlyList<string>? Reviews { get; private set; }
    public string? ContentSize { get; private set; }

    public virtual IBookProduct GetBookProduct()
        => this;

    public void SetRating(float rating)
    {
        Rating = rating;
    }

    public void SetReviews(IReadOnlyList<string> reviews)
    {
        Reviews = reviews;
    }
}