﻿using Core.Enums;

namespace Core.Contracts;

public interface IBookProduct
{
    BookTypeEnum Type { get; }
    string? Isbn { get; }
    string? Title { get; }
    string? Author { get; }
    decimal? Price { get; }
    public float? Rating { get; }
    IReadOnlyList<string> Reviews { get; }
    void SetRating(float rating);
    void SetReviews(IReadOnlyList<string> reviews);
}