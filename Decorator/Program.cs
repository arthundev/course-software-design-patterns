﻿using Core.Books;
using Decorator.Impl;
using Infrastructure.DataSource;
using Infrastructure.JsonSerializer;
using System.Diagnostics;

const string printedBookIsbn = "978-0-307-45563-2";
var printedBook = BookProductDataSource.GetSingleBookProduct<PrintedBook>(printedBookIsbn);
Debug.WriteLine(JsonSerializerConfig.Serialize(printedBook.GetBookProduct()));

var printedBookWithRating = new BookProductWithRatingDecorator(printedBook);
var printedBookWithContentSize = new BookProductWithReviewsDecorator(printedBookWithRating);
Debug.WriteLine(JsonSerializerConfig.Serialize(printedBookWithContentSize.GetBookProduct()));