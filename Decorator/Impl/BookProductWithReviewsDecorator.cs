﻿using Core.Books;
using Core.Contracts;
using Infrastructure.DataSource;

namespace Decorator.Impl;

public class BookProductWithReviewsDecorator : BookProductDecorator
{
    public BookProductWithReviewsDecorator(BookProduct wrappee) : base(wrappee)
    {
    }

    public override IBookProduct GetBookProduct()
    {
        // business logic to get book reviews
        var bookProductReviews = BookProductReviewDataSource.GetBookProductReviews(BookProduct.Isbn);
        BookProduct.SetReviews(bookProductReviews);
        return base.GetBookProduct();
    }
}