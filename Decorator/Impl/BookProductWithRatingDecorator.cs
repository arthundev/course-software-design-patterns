﻿using Core.Books;
using Core.Contracts;
using Infrastructure.DataSource;

namespace Decorator.Impl;

public class BookProductWithRatingDecorator : BookProductDecorator
{
    public BookProductWithRatingDecorator(BookProduct wrappee) : base(wrappee)
    {
    }

    public override IBookProduct GetBookProduct()
    {
        // business logic to get book rating
        var bookProductRating = BookProductRatingDataSource.GetBookProductRating(BookProduct.Isbn);
        BookProduct.SetRating(bookProductRating.Average);
        return base.GetBookProduct();
    }
}