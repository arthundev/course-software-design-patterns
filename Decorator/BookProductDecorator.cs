﻿using Core.Books;
using Core.Contracts;

namespace Decorator;

public abstract class BookProductDecorator : BookProduct
{
    private readonly BookProduct _wrappee;
    protected IBookProduct BookProduct { get; }

    protected BookProductDecorator(BookProduct wrappee)
    {
        _wrappee = wrappee;
        BookProduct = wrappee is BookProductDecorator decorator ? decorator._wrappee : wrappee;
    }

    public override IBookProduct GetBookProduct()
        => _wrappee.GetBookProduct();
}