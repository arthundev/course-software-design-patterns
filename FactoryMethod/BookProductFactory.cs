﻿using Core.Contracts;
using Core.Enums;
using Infrastructure.DataSource;

namespace FactoryMethod;

public abstract class BookProductFactory<T> where T : IBookProduct
{
    protected abstract T CreateBookProduct(string isbn);

    public T GetBookProduct(string isbn)
    {
        return CreateBookProduct(isbn);
    }

    protected BookProductData GetBookProductFromDataSource(string isbn, BookTypeEnum bookType)
    {
        var bookProduct = BookProductDataSource.GetSingleBookProduct(isbn, bookType);
        if (bookProduct != null)
            return bookProduct;
        throw new Exception($"Book with ISBN '{isbn}' and type '{bookType}' was not found.");
    }
}