﻿using Core.Books;
using Core.Enums;

namespace FactoryMethod.Impl;

public class PrintedBookFactory : BookProductFactory<PrintedBook>
{
    protected override PrintedBook CreateBookProduct(string isbn)
    {
        // business logic to get book entity
        var bookProduct = GetBookProductFromDataSource(isbn, BookTypeEnum.PrintedBook);
        var printedBook = new PrintedBook
        {
            Isbn = bookProduct.Isbn,
            Title = bookProduct.Title,
            Author = bookProduct.Author,
            Price = bookProduct.Price,
            PageCount = bookProduct.PageCount ?? 0
        };
        return printedBook;
    }
}