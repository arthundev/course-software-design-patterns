﻿using Core.Books;
using Core.Enums;

namespace FactoryMethod.Impl;

public class AudioBookFactory : BookProductFactory<AudioBook>
{
    protected override AudioBook CreateBookProduct(string isbn)
    {
        // business logic to get book entity
        var bookProduct = GetBookProductFromDataSource(isbn, BookTypeEnum.AudioBook);
        var audioBook = new AudioBook
        {
            Isbn = bookProduct.Isbn,
            Title = bookProduct.Title,
            Author = bookProduct.Author,
            Price = bookProduct.Price,
            RunningTime = bookProduct.RunningTime ?? 0
        };
        return audioBook;
    }
}