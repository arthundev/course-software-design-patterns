﻿using FactoryMethod.Impl;
using Infrastructure.JsonSerializer;
using System.Diagnostics;

const string printedBookIsbn = "978-0-307-45563-2";
const string audioBookIsbn = "978-0-7679-0342-7";

var printedBookFactory = new PrintedBookFactory();
var printedBook = printedBookFactory.GetBookProduct(printedBookIsbn);
Debug.WriteLine(JsonSerializerConfig.Serialize(printedBook));

var audioBookFactory = new AudioBookFactory();
var audioBook = audioBookFactory.GetBookProduct(audioBookIsbn);
Debug.WriteLine(JsonSerializerConfig.Serialize(audioBook));